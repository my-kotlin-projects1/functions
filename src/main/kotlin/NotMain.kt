fun main() {
    val scriptedString = "F2p)v\"y233{0->c}ttelciFc"
    println(firstHalfDescript(scriptedString) + secondHalfDescript(scriptedString))
    println(shift(scriptedString, 1))
    val resultString = shift2(scriptedString){ scriptedString.map { c -> c + 1}.joinToString("")}
    println(resultString)
}

fun firstHalfDescript(string: String): String{
    return string.substring(0, string.length/2)
        .map { c -> c + 1}.joinToString("")
        .replace('5', '2')
        .replace('4', 'u')
        .map { c -> c - 3 }.joinToString("")
        .replace('0', 'o')

}

fun secondHalfDescript(string: String): String{
    var descrString = string.removeRange(0, string.count()/2)
    descrString = descrString.reversed()
    descrString = shift2(descrString){ it.map { c -> c - 4}.joinToString("")}
    descrString = descrString.replace('_', ' ')
    return descrString
}

fun shift(string: String, chars: Int): String{
    return string.map {char -> char + chars}.joinToString("")
}

fun shift2(str: String, funShift: (String) -> String): String {
    return funShift(str)
}