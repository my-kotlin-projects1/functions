fun main() {
    val itemsOfManyColors = listOf(
        listOf("red apple", "green apple", "blue apple"),
        listOf("red fish", "blue fish"),
        listOf("yellow banana", "teal banana"))
    val redItem = itemsOfManyColors.flatten().filter { it.contains("red") }
    println(redItem)

    val numbers = listOf(7, 4, 8, 4, 3, 22, 18, 11)
    val primes = numbers.filter { number ->
        (2 until number).map { number % it }
            .none { it == 0 }
    }
    print(primes)
}