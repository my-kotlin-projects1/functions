fun main() {
    val animals = listOf("zebra", "giraffe", "elephant", "rat")
    val babies = animals
        .map{ animal -> "A baby $animal" }
        .map{ baby -> "$baby, with the cutest little tail ever!"}
    println(babies)

    val tenDollarWords = listOf("auspicious", "avuncular", "obviate")
    val tenDollarWordLengths = tenDollarWords.map { it.length }
    print(tenDollarWordLengths)
}

