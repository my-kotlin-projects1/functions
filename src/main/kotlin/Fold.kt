fun main() {
    val foldedValue = listOf(1, 2, 3, 4).fold(0) { accumulator, number ->
        println("Accumulated value: $accumulator")
        accumulator + number * 3
    }
    println("Final value: $foldedValue")
}