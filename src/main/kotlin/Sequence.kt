import kotlin.system.measureNanoTime

fun main() {
    val listInNanos = measureNanoTime { val toList = (1..7919).toList().filter { it.isPrime() }.take(1000)
        println(toList.size) }
    println("список обработан за $listInNanos наносекунд")

    val sequenceInNanos = measureNanoTime {
        val oneThousandPrimes = generateSequence(3) {
                value -> value + 1
        }.filter { it.isPrime() }.take(1000)
    }
    println("последовательность обработана за $sequenceInNanos наносекунд")
}

fun Int.isPrime(): Boolean {
    (2 until this).map {
        if (this % it == 0) {
            return false
        }
    }
    return true
}
