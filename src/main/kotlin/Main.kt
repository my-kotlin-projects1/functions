fun main() {
    val juniorGrossSalary = 50_000
    val middleGrossSalary = 134_000
    val seniorGrossSalary = 167_000

//    val juniorNetSalary = juniorGrossSalary * 0.87
//    val juniorBank = juniorNetSalary * 0.3 * 12
//    println("With a salary of $juniorGrossSalary person can accumulate $juniorBank in 1 year")

    calculateSavings(middleGrossSalary, rate = 0.5, period = 6)
    calculateSavings(middleGrossSalary)
    calculateSavings(middleGrossSalary, 0.5, 24)

    val sum = calculateSavings(35000, 0.12, 8) +
              calculateSavings(55000, 0.2, 8)
    println(sum)

    val sum1 = calculateSavings2(35000, 0.8, 12, printInfo = {salary, rate, period, bank ->
        println("First case bank $bank") })
    val sum2 = calculateSavings2(55000, 0.8, 12) {_, _, _, bank ->
        println("Second case bank $bank") }

    val sum3 = calculateSavings2(75000, 0.8, 12, printInfo = {salary, rate, period, bank ->
        println("Salary: $salary, rate: ${rate*100}%, period: $period monthes, accumulation: $bank") })
    println(sum3)

    println(multiply(17, 23))
    println(multiply(2, 2, 2))

    val anonimus: (Int) -> Unit = fun(a: Int) { println(a)}
    anonimus(1231892498)
// Типы объявлений лямбд
    val lambda = { println("Tydysh!!")}
    val lambdaWithParams = { message: String -> println(message)}
    val lambdaWithParamsAndReturn = { a: Int, b: Int -> a * b}
// вызов лямбд
    lambda()
    lambdaWithParams("Fucken!")
    println(lambdaWithParamsAndReturn(9, 4))

// задачка на подсчёт количества символов в строке
    val str = "This is a typical task for developer interview"
    println(str.count { char -> checkSymbol(char) }) // через доп функцию.
    println(str.count { checkSymbol(it) })  // через доп функцию.
    println(str.count { it == 'e' })  // через встройку.

// убрать из этой же строки все гласные буквы
    val vowels = "aeoiu"
    println(str.filter { it !in vowels })
}



fun calculateSavings(salary: Int, rate: Double = 0.3, period: Int = 12): Double {
    val bank = salary * 0.87 * rate * period
    println("Salary: $salary, rate: ${rate*100}%, period: $period monthes, accumulation: $bank")
    return bank
}

// пример функции высшего порядка.
fun calculateSavings2(
    salary: Int,
    rate: Double = 0.3,
    period: Int = 12,
    printInfo: ((salary: Int, rate: Double, period: Int, bank: Double) -> Unit)? = null
): Double {
    val netSalary = salary * 0.87
    val bank = netSalary * 0.87 * rate * period
    printInfo?.invoke(salary, rate, period, bank)
    return bank
}

fun multiply(a: Int, b: Int) = a * b
fun multiply(a: Int, b: Int, c: Int) = a * b * c

fun checkSymbol(char: Char): Boolean = char == 'e'
