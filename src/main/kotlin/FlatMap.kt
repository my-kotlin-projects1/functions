fun main() {
    val card1 = Card()
    println(card1.card)
    card1.checkNumber2(34)
    println(card1.checkNumber3(35))
}

class Card {
    var card = makeCard()

    fun makeCard(): MutableList<List<Int>> {
        val dopList = (1..90).toList().shuffled()
        val card: MutableList<List<Int>> = MutableList(3){ List(4) { 0 } }
        card[0] += dopList.subList(0, 5)
        card[0] = card[0].shuffled()
        card[1] += dopList.subList(6, 11)
        card[1] = card[1].shuffled()
        card[2] += dopList.subList(12, 17)
        card[2] = card[2].shuffled()
        return card
    }

    // смешно, эволюция кода))
    fun checkNumber(number: Int): Boolean{
        var check: Boolean = false
        this.card.forEach {
            check = number in it
            if (number in it) { println ("Совпало $number")}
        }
        return check
    }

    fun checkNumber2(number: Int): Boolean{
        val flatCard = this.card.flatMap { it }
        if (number in flatCard) println ("Совпало $number")
        return number in flatCard
    }

    fun checkNumber3(number: Int) = number in this.card.flatten()
}